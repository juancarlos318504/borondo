﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public bool acelerando ;
    public bool frenando ;
    public bool girandoDer ;
    public bool girandoIzq;
    public float throttle = 0;
    public float steer = 0;
    

    public float acelerationValue = 5; //este valor se podria poner en cada boton para diferenciar la aceleracion.
    AudioSource audioN;
    public AudioClip acelerandoSound;
    public AudioClip idleSound;

    private bool idle = false;
    private bool ace = false;

    void Start()
    {
        audioN = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

        if (acelerando || frenando)
        {
            if (!ace)
            {
                audioN.clip = acelerandoSound;
                audioN.Play();
                ace = true;
                idle = false;
            }
                
        } 

        //Control de avance
        if (throttle < 1 && acelerando)
        {
            throttle += acelerationValue * Time.deltaTime;
        }
        else if(throttle >= 1 && acelerando)
        {
            throttle = 1;
        }
        else if (throttle <= 1 && !acelerando && throttle >=0)
        {
            throttle -= acelerationValue * Time.deltaTime;
        }

        


        if (throttle > -1 && frenando)
        {
            throttle -= acelerationValue * Time.deltaTime;
        }
        else if(throttle <= -1 && frenando)
        {
            throttle = -1;
        }
        else if (throttle >= -1 && !frenando && throttle <= 0)
        {
            throttle += acelerationValue * Time.deltaTime;
        }

        if (!acelerando && !frenando && (throttle <= 0 + acelerationValue * Time.deltaTime && throttle >= 0 - acelerationValue * Time.deltaTime))
        {
            throttle = 0;
            if (!idle)
            {
                audioN.clip = idleSound;
                audioN.Play();
                idle = true;
                ace = false;
            }
            
            
        }






        //Control de giro
        if (steer < 1 && girandoDer)
        {
            steer += acelerationValue * Time.deltaTime;
        }
        else if (steer >= 1 && girandoDer)
        {
            steer = 1;
        }
        else if (steer <= 1 && !girandoDer && steer >0)
        {
            steer -= acelerationValue * Time.deltaTime;
        }


        if (steer > -1 && girandoIzq)
        {
            steer -= acelerationValue * Time.deltaTime;
        }
        else if (steer <= -1 && girandoIzq)
        {
            steer = -1;
        }

        else if (steer >= -1 && !girandoIzq && steer <0)
        {
            steer += acelerationValue * Time.deltaTime;
        }

        if (!girandoIzq && !girandoDer && (steer <= 0 + acelerationValue * Time.deltaTime && steer >= 0 - acelerationValue * Time.deltaTime))
        {
            steer = 0; //
        }



        /*if (!girandoDer && !girandoIzq)
        {
            steer = 0;
        }*/








        //throttle = Input.GetAxis("Vertical");
        //steer = Input.GetAxis("Horizontal");
    }
}
