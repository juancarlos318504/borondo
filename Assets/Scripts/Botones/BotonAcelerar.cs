﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BotonAcelerar : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Sprite FirstImage; 
    public Sprite SecondImage;

    private GameObject chiva;

    void Awake()
    {
        chiva = GameObject.Find("chivaArmado");
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        GetComponent<Image>().sprite = SecondImage;
        SelectPlaneToPlay.ChivaControl.GetComponent<InputManager>().acelerando = true;
        transform.localScale = new Vector3(.9f, .9f, .9f);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        SelectPlaneToPlay.ChivaControl.GetComponent<InputManager>().acelerando = false;
        GetComponent<Image>().sprite = FirstImage;
        transform.localScale = new Vector3(1, 1, 1);
    }
}
