﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BotonFrenar : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Sprite FirstImage;
    public Sprite SecondImage;

    public void OnPointerDown(PointerEventData eventData)
    {
        SelectPlaneToPlay.ChivaControl.GetComponent<InputManager>().frenando = true; //innputmanager y static een el manager
        GetComponent<Image>().sprite = SecondImage;
        transform.localScale = new Vector3(.9f, .9f, .9f);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        SelectPlaneToPlay.ChivaControl.GetComponent<InputManager>().frenando = false;
        GetComponent<Image>().sprite = FirstImage;
        transform.localScale = new Vector3(1, 1, 1);
    }
}

