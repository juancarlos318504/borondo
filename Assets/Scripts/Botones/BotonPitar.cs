﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BotonPitar : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Sprite FirstImage;
    public Sprite SecondImage;
    AudioSource audioN;

    void Start()
    {
        audioN = GetComponent<AudioSource>();
        audioN.Pause();
        
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        audioN.Play();
        GetComponent<Image>().sprite = SecondImage;
        transform.localScale = new Vector3(.9f, .9f, .9f);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        audioN.Stop();
        GetComponent<Image>().sprite = FirstImage;
        transform.localScale = new Vector3(1, 1, 1);
    }
}