﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FlyBotonDerecha : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    //public Sprite FirstImage;
    //public Sprite SecondImage;
    
    public void OnPointerDown(PointerEventData eventData)
    {
        PowerUp.chivaAvion.GetComponent<InputManager>().girandoDer = true;
        
        //GetComponent<Image>().sprite = SecondImage;
        transform.localScale = new Vector3(.9f, .9f, .9f);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        PowerUp.chivaAvion.GetComponent<InputManager>().girandoDer = false;
        //GetComponent<Image>().sprite = FirstImage;
        transform.localScale = new Vector3(1, 1, 1);
    }
}
