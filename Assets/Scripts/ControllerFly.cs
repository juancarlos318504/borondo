﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerFly : MonoBehaviour
{
    //public InputManager im;
    public float speed = 0.0001f;
    private VariableJoystick joystick;
    public static bool changingMode;
    public static Vector3 chivaPosition;
    public static Quaternion chivaRotation;
    

    public static bool looking = false;
    public static bool runing = false;
    public static bool roting = false;

    //public static ParticleSystem [] propulsors;
    public static ParticleSystem propulsor1;
    public static ParticleSystem propulsor2;
    public static ParticleSystem propulsor3;
    public static ParticleSystem propulsor4;

    public static bool pause = false;


    void Awake()
    {
        //im = GetComponent<InputManager>();
        joystick = GameObject.Find("Canvas").transform.Find("SafeArea").transform.Find("Controles").transform.Find("Variable Joystick").GetComponent<VariableJoystick>();
        propulsor1 = gameObject.transform.Find("Llanta1").Find("Particle System").GetComponent<ParticleSystem>();
        propulsor2 = gameObject.transform.Find("Llanta2").Find("Particle System").GetComponent<ParticleSystem>();
        propulsor3 = gameObject.transform.Find("LLanta3").Find("Particle System").GetComponent<ParticleSystem>();
        propulsor4 = gameObject.transform.Find("LLanta4").Find("Particle System").GetComponent<ParticleSystem>();


        chivaPosition = new Vector3(0, 0, 0);
        chivaRotation = Quaternion.identity;
        changingMode = false;
        looking = true;
        runing = true;
        roting = false;

        stopPropulsors();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (!GetComponent<Animation>().IsPlaying("Take 001") && !changingMode && !pause) //!transform.Find("chivaAvion1").GetComponent<Animation>().IsPlaying("Take 001")
        {

            playPropulsors();
            //GetComponent<AudioSource>().Play();
            transform.position += -transform.right * Time.deltaTime * speed;
            speed -= transform.forward.y * Time.deltaTime * 10f;

            if (speed < 0.5f)
            {
                speed = 0.5f;
            }

            if (speed > 1.0f)
            {
                speed = 1.0f;
            }

            //transform.Rotate(im.steer, 0f, -im.throttle, Space.Self); //joystick.Direction.y
            transform.Rotate(0f, joystick.Direction.x*2, -joystick.Direction.y*2, Space.Self);
            //transform.Rotate(-joystick.Direction.y, joystick.Direction.x * 2, 0f, Space.Self);
        }

        else if (changingMode)
        {
            Vector3 relativePosition = chivaPosition - transform.position;

            /*Vector3 newDirection = Vector3.RotateTowards(transform.forward, relativePosition, 1 * Time.deltaTime, 0.0f);
            transform.rotation = Quaternion.LookRotation(newDirection);
            transform.rotation = Quaternion.LookRotation(newDirection);*/ //Esta es la otra manera pero no funciona la suma de los 90 grados

            Quaternion vectorLook = Quaternion.LookRotation(relativePosition.normalized)*Quaternion.Euler(0,90,0);
            

            
            if(Quaternion.Angle(transform.rotation, vectorLook) >= 1 && looking && !roting )
            {
                Quaternion Direction = Quaternion.RotateTowards(transform.rotation, vectorLook, 80 * Time.deltaTime);
                transform.rotation = Direction;
            }

            if (Quaternion.Angle(transform.rotation, vectorLook) < 1 && runing)
            {
                transform.position = Vector3.MoveTowards(transform.position, new Vector3(chivaPosition.x, chivaPosition.y + 0.2f, chivaPosition.z), 1 * Time.deltaTime);
            }

            if (transform.position.x == chivaPosition.x && transform.position.z == chivaPosition.z)
            {
                looking = false;
                runing = false;
                roting = true;
                transform.rotation = Quaternion.RotateTowards(transform.rotation, chivaRotation, 80 * Time.deltaTime);
                
            }

            if (Quaternion.Angle(transform.rotation, chivaRotation) < 1 && roting)
            {
                transform.position = Vector3.MoveTowards(transform.position, chivaPosition, 0.3f * Time.deltaTime);
                GetComponent<AudioSource>().Stop();
                stopPropulsors();
            }

        }
    }

    public static void stopPropulsors()
    {
        propulsor1.Stop();
        propulsor2.Stop();
        propulsor3.Stop();
        propulsor4.Stop();
    }

    public static void playPropulsors()
    {
        propulsor1.Play();
        propulsor2.Play();
        propulsor3.Play();
        propulsor4.Play();
    }

    public void pauseFly()
    {
        pause = true;
    }

    public void playFly()
    {
        pause = false;
    }

    void FixedUpdate()
    {
        
    }

    
}
