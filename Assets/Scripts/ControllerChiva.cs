﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerChiva : MonoBehaviour
{
    //public InputManager im;
    private VariableJoystick joystick;
    public float speed = 0.5f;
    public float TurnSpeed = 10f;
    public float maxTurn = 25f;

    public List<GameObject> throttlewheels;
    public List<GameObject> steeringwheels;

    private Rigidbody rb;
    private Vector3 previousPosition = Vector3.zero;


    void Start()
    {
        //im = GetComponent<InputManager>();
        rb = GetComponent<Rigidbody>();
        joystick = GameObject.Find("Canvas").transform.Find("SafeArea").transform.Find("Controles").transform.Find("Variable Joystick").GetComponent<VariableJoystick>();
    }

    void Update()
    {

        //transform.Translate(Vector3.left * Time.deltaTime * im.throttle * speed);
        //transform.Rotate(Vector3.up, Time.deltaTime * im.steer * im.throttle * maxTurn * TurnSpeed, Space.Self);

        transform.Translate(Vector3.left * Time.deltaTime * joystick.Direction.y * speed);
        transform.Rotate(Vector3.up, Time.deltaTime * joystick.Direction.y * joystick.Direction.x * maxTurn * TurnSpeed, Space.Self);
        GetComponent<AudioSource>().volume = 0.1f * Mathf.Abs( joystick.Direction.y) + 0.1f;

        //var velocity = ((transform.position - previousPosition).magnitude/Time.deltaTime);
        previousPosition = transform.position;
        foreach (GameObject mesh in throttlewheels)
        {
            //mesh.transform.localEulerAngles = new Vector3 (0f, 0f, mesh.transform.localEulerAngles.z + velocity * (im.throttle >= 0 ? 1 : -1) ); //   / (2 * Mathf.PI * 0.01f)
            //mesh.transform.Rotate(0f, 0f, velocity * (im.throttle >= 0 ? 1 : -1) / (2 * Mathf.PI * 0.01f));

            //mesh.transform.Rotate(0f, 0f, im.throttle * speed / (2 * Mathf.PI * 0.01f));
            mesh.transform.Rotate(0f, 0f, joystick.Direction.y * speed / (2 * Mathf.PI * 0.01f));//
        }
        
        foreach (GameObject wheel in steeringwheels)
        {
            //wheel.transform.localEulerAngles = new Vector3(wheel.transform.localEulerAngles.x, im.steer * maxTurn, wheel.transform.localEulerAngles.z);
            wheel.transform.localEulerAngles = new Vector3(wheel.transform.localEulerAngles.x, joystick.Direction.x * maxTurn, wheel.transform.localEulerAngles.z);
        }

    }
}
