using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.ARFoundation;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
//using Lean.Transition.Examples;
//using Lean.Gui;

public class ImageRecognition : MonoBehaviour
{
    private ARTrackedImageManager _arTrackedImageManager;
    private GameObject chiva;
    private GameObject indicators;
    private bool anim ;

    public FlexibleColorPicker colorPicker;
    public Button ContinueButton;
    //public GameObject ModalInstructions;
    public GameObject HelpButton;
    public Text mensaje;
    public GameObject PanelImage;

    public Sprite imageNino;
    public Sprite imageNina;

    public GameObject Lights;
    private void Awake(){
        _arTrackedImageManager = GetComponent<ARTrackedImageManager>();
        chiva = GameObject.Find("chivaArmado");
        indicators = chiva.transform.Find("Indicators").gameObject;
        //indicators = GameObject.Find("Indicators");//

        chiva.SetActive(false);
        indicators.SetActive(false);
        ContinueButton.gameObject.SetActive(false);
        colorPicker.gameObject.SetActive(false);
        DontDestroyOnLoad(chiva);
    }

    void Start()
    {
        Button btn = ContinueButton.GetComponent<Button>();
        btn.onClick.AddListener(ChangeScene);

        //ModalInstructions.GetComponent<LeanWindow>().TurnOn();
        ///HelpButton.SetActive(false);
        anim = true;
        HelpButton.SetActive(true);
        //_arTrackedImageManager.enabled=false;

        //HelpButton.GetComponent<Animation>().Play(); 
        //HelpButton.GetComponent<Animation>().Play("UIElementAnimation");

        PanelImage.GetComponent<Image>().sprite = imageNino;
    }

    void ChangeScene()
    {
        //Handheld.Vibrate();
        //SceneManager.LoadScene("ChivaDriving");
        Initiate.Fade("ChivaDriving", Color.black, 1.0f);
    }

    void OnEnable(){     
        _arTrackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;             
    }

    void OnDisable(){
        _arTrackedImageManager.trackedImagesChanged -= OnTrackedImagesChanged;      
    }

    void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs){
        
        foreach (ARTrackedImage trackedImage in eventArgs.updated)
        {
            updateARImage(trackedImage);
        }

        foreach (ARTrackedImage trackedImage in eventArgs.added)
        {
            updateARImage(trackedImage);
        }

        foreach (ARTrackedImage trackedImage in eventArgs.removed)
        {
            //chiva.SetActive(false);
        }
        
    }

    private void updateARImage(ARTrackedImage trackedImage){
        
        if (anim)
        {
            chiva.GetComponent<Animation>()["Armado"].speed = 2.0f;
            chiva.GetComponent<Animation>()["Armado"].normalizedTime = 0.041f;
            chiva.GetComponent<Animation>().Play("Armado");

            chiva.SetActive(true);
            anim = false;
        }

        if (chiva.GetComponent<Animation>().IsPlaying("Armado"))
        {
            indicators.SetActive(false);
        }
        else
        {
            indicators.SetActive(true);
            ContinueButton.gameObject.SetActive(true);
            colorPicker.gameObject.SetActive(true);

            
            HelpButton.GetComponent<Animator>().enabled = true;
            PanelImage.GetComponent<Image>().sprite = imageNina;
            mensaje.GetComponent<Text>().text = "<color=#F16622><size=80>Hora de darle vida a tu chiva</size></color> \n\n <color=#006666>Pulsa sobre los indicadores y mueve las barras de color para darle color a cada parte de tu chiva \n\n Cuando est�s satisfecho con los colores puedes pulsar sobre el <b>bot�n continuar</b></color>";
        }

        Lights.transform.position = chiva.transform.position;
        Lights.transform.rotation = chiva.transform.rotation;
        chiva.transform.position=trackedImage.transform.position;
        chiva.transform.rotation=trackedImage.transform.rotation;
        //indicators.transform.position = trackedImage.transform.position;
        //indicators.transform.rotation = trackedImage.transform.rotation;
    }

}
