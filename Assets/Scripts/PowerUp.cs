﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    public static GameObject chiva;
    public static GameObject chivaAvion;

    void Awake()
    {
        //chiva = GameObject.Find("chivaArmado");
        chivaAvion = GameObject.Find("chivaAvion");
        chiva = GameObject.Find("chivaArmado");
        chivaAvion.SetActive(false);
    }

    void OnTriggerEnter(Collider collider)
    {     
        if (collider.gameObject.name == "chivaArmado")
        {
            
            chivaAvion.transform.position = collider.transform.position;
            chivaAvion.transform.rotation = collider.transform.rotation;
            chivaAvion.SetActive(true);
            collider.gameObject.SetActive(false);
            gameObject.SetActive(false);
            chivaAvion.SetActive(true);
            

            //chiva.GetComponent<Animation>()["Armado"].normalizedTime = 0.041f;
            chivaAvion.GetComponent<Animation>().Play("ArmadoAvion");
            chiva = GameObject.Find("chivaAvion");
        }
        
    }
}
