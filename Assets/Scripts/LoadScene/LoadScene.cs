﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    public float delay = 3;
    
    void Start()
    {
        StartCoroutine(ChangeToScene());
    }

    // Update is called once per frame
    public IEnumerator ChangeToScene()
    {
        yield return new WaitForSeconds(delay);
        changeScene();
    }

    public void changeScene()
    {
        Initiate.Fade("Personalization", Color.black, 1.0f);
    }

}
