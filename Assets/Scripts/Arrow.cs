﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Transition.Examples;

public class Arrow : MonoBehaviour
{
    public GameObject chivaavion;
    // Start is called before the first frame update
    void Start()
    {
        //transform.rotation = Quaternion.LookRotation(chivaavion.transform.right, chivaavion.transform.up);
        //transform.localScale = Vector3.zero;
        GetComponent<Animation>().Play();
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(chivaavion.transform.position, chivaavion.transform.up);
        transform.RotateAround(transform.position, transform.right, 90);
    }

    public void openArrow()
    {
        GetComponent<Animation>()["Arrow"].normalizedTime = 0f;
        GetComponent<Animation>()["Arrow"].speed = 1;
        GetComponent<Animation>().Play("Arrow");
    }

    public void closeArrow()
    {
        GetComponent<Animation>()["Arrow"].normalizedTime = 1f;
        GetComponent<Animation>()["Arrow"].speed = -1;
        GetComponent<Animation>().Play("Arrow");
    }
}
