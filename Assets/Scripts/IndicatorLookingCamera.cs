﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RDG;



public class IndicatorLookingCamera : MonoBehaviour
{
    AudioSource audioN;//

    void Start()
    {
        audioN = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.LookRotation(-Camera.main.transform.forward, Camera.main.transform.up);
    }

    void OnMouseDown()
    {
        Vibration.Vibrate(300);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
        ChangeColors.pieceName = this.gameObject.name;
        GameObject[] indicators = GameObject.FindGameObjectsWithTag("Indicator");
        audioN.Play();
        
        foreach (var indicator in indicators)
        {
            indicator.transform.localScale = new Vector3(0.02f, 0.02f, 0.02f);
            indicator.GetComponent<Animator>().enabled = true;
        }
        transform.localScale = new Vector3(0.04f, 0.04f, 0.04f);
        GetComponent<Animator>().enabled = false;
    }
}
