﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Velocimetro : MonoBehaviour
{
    private float angle;
    public VariableJoystick joystick;

    void Start()
    {
        angle = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //var throttle = Mathf.Abs(GameObject.Find("chivaArmado").GetComponent<InputManager>().throttle);
        var throttle = Mathf.Abs(joystick.Direction.y);

        if (throttle < 1)
        {
            angle = throttle * -180;
        }
        else if(throttle >= 1 && angle > -200)
        {
            angle = angle - 2*Time.deltaTime;
        }
        else if (throttle >= 1 && angle <= -200)
        {
            angle = -200;
        }
        transform.localEulerAngles = new Vector3(transform.rotation.x, transform.rotation.y, angle);
    }
}
