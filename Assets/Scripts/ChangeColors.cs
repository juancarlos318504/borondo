﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Gui;
using UnityEngine.XR.ARFoundation;

public class ChangeColors : MonoBehaviour
{
    //private GameObject chiva = ImageRecognition.chiva;
    public GameObject chiva;
    public FlexibleColorPicker colorPicker;
    public static string pieceName;
    public GameObject ModalInstructions;
    public GameObject HelpButton;

    void Start()
    {
        colorPicker.startingColor = chiva.transform.Find("techo1").gameObject.GetComponent<Renderer>().material.color;
        pieceName = "none";
        HelpButton.SetActive(false);
        ModalInstructions.GetComponent<LeanWindow>().TurnOn();
        //GetComponent<ImageRecognition>().enabled = false;
        //GetComponent<ARTrackedImageManager>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        switch (pieceName)
        {
            case "none":
                break;

            case "CapoIndicator":
                //colorPicker.color = chiva.transform.Find("Capo").Find("capo1").gameObject.GetComponent<Renderer>().material.color;
                chiva.transform.Find("Capo").Find("capo1").gameObject.GetComponent<Renderer>().material.SetColor("_Color", colorPicker.color);
                chiva.transform.Find("base5").gameObject.GetComponent<Renderer>().material.SetColor("_Color", colorPicker.color);
                chiva.transform.Find("meson").gameObject.GetComponent<Renderer>().material.SetColor("_Color", colorPicker.color);
                break;

            case "PilaresIndicator":
                chiva.transform.Find("pilar1").gameObject.GetComponent<Renderer>().material.SetColor("_Color", colorPicker.color);
                chiva.transform.Find("pilar2").gameObject.GetComponent<Renderer>().material.SetColor("_Color", colorPicker.color);
                chiva.transform.Find("pilar3").gameObject.GetComponent<Renderer>().material.SetColor("_Color", colorPicker.color);
                chiva.transform.Find("pilar4").gameObject.GetComponent<Renderer>().material.SetColor("_Color", colorPicker.color);

                break;

            case "TechoIndicator":                
                chiva.transform.Find("techo1").gameObject.GetComponent<Renderer>().material.SetColor("_Color", colorPicker.color);
                break;

            case "AsientoIndicator":
                chiva.transform.Find("asiento1").gameObject.GetComponent<Renderer>().material.SetColor("_Color", colorPicker.color);
                chiva.transform.Find("asiento2").gameObject.GetComponent<Renderer>().material.SetColor("_Color", colorPicker.color);
                chiva.transform.Find("asiento3").gameObject.GetComponent<Renderer>().material.SetColor("_Color", colorPicker.color);
                break;

            case "Techo2Indicator":
                chiva.transform.Find("techo2").gameObject.GetComponent<Renderer>().material.SetColor("_Color", colorPicker.color);
                break;

            case "TapaIndicator":
                chiva.transform.Find("Capo").Find("Capo2").gameObject.GetComponent<Renderer>().material.SetColor("_Color", colorPicker.color);
                chiva.transform.Find("Capo").Find("capoR1").gameObject.GetComponent<Renderer>().material.SetColor("_Color", colorPicker.color);
                chiva.transform.Find("Capo").Find("capoR2").gameObject.GetComponent<Renderer>().material.SetColor("_Color", colorPicker.color);
                chiva.transform.Find("Capo").Find("capoR3").gameObject.GetComponent<Renderer>().material.SetColor("_Color", colorPicker.color);
                break;

            case "LlantasIndicator":
                chiva.transform.Find("Llanta1").gameObject.GetComponent<Renderer>().materials[0].SetColor("_Color", colorPicker.color);
                chiva.transform.Find("Llanta2").gameObject.GetComponent<Renderer>().materials[0].SetColor("_Color", colorPicker.color);
                chiva.transform.Find("LLanta3").gameObject.GetComponent<Renderer>().materials[0].SetColor("_Color", colorPicker.color);
                chiva.transform.Find("LLanta4").gameObject.GetComponent<Renderer>().materials[0].SetColor("_Color", colorPicker.color);
                chiva.transform.Find("manubrio").gameObject.GetComponent<Renderer>().materials[0].SetColor("_Color", colorPicker.color);
                break;

            case "EscaleraIndicator":
                chiva.transform.Find("escalera").gameObject.GetComponent<Renderer>().materials[0].SetColor("_Color", colorPicker.color);
                break;

            case "BaseIndicator":
                chiva.transform.Find("base1").gameObject.GetComponent<Renderer>().materials[0].SetColor("_Color", colorPicker.color);
                chiva.transform.Find("base2").gameObject.GetComponent<Renderer>().materials[0].SetColor("_Color", colorPicker.color);
                chiva.transform.Find("base3").gameObject.GetComponent<Renderer>().materials[0].SetColor("_Color", colorPicker.color);
                chiva.transform.Find("base4").gameObject.GetComponent<Renderer>().materials[0].SetColor("_Color", colorPicker.color);
                break;
            case "FrenteIndicator":
                chiva.transform.Find("base_luz").gameObject.GetComponent<Renderer>().material.SetColor("_Color", colorPicker.color); 
                break;
        }
        
        /*Transform mychildtransform = chiva.transform.Find("Capo");
        Transform mychildtransform1 = mychildtransform.Find("capo1");
        GameObject mychildgameobject = mychildtransform1.gameObject;

        Renderer rend = mychildgameobject.GetComponent<Renderer>();
        //rend.material.shader = Shader.Find("Specular");
        rend.material.SetColor("_Color", colorPicker.color);*/
    }
}
