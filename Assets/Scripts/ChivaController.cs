﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(InputManager))]
[RequireComponent(typeof(Rigidbody))]
public class ChivaController : MonoBehaviour
{
    public InputManager im;
    public List<WheelCollider> throttlewheels;
    public List<GameObject> steeringwheels;
    public float strengthCoefficient = 2000f;
    public float maxTurn = 20f;

    public List<GameObject> meshes;

    public Transform CM;
    public Rigidbody rb;

    void Start()
    {
        im = GetComponent<InputManager>();
        rb = GetComponent<Rigidbody>();

        if (CM)
        {
            rb.centerOfMass = CM.position;
        }
    }

    // Update is called once per frame
    void Update()
    {
        foreach(WheelCollider wheel in throttlewheels)
        {

            wheel.motorTorque = strengthCoefficient * Time.deltaTime * im.throttle; 
        }

        foreach(GameObject wheel in steeringwheels)
        {
            wheel.GetComponent<WheelCollider>().steerAngle = maxTurn * im.steer;
            wheel.transform.localEulerAngles = new Vector3(0f, im.steer * maxTurn, 0f);
        }

        foreach(GameObject mesh in meshes)
        {
            mesh.transform.Rotate(0f, 0f, rb.velocity.magnitude * (transform.InverseTransformDirection(rb.velocity).z >= 0 ? 1: -1) / (2 * Mathf.PI * 0.01f));
        }
    }
}
